/* 
    - This file captures the create react-app built in config, allowing it to be altered or edited before it is consumed by webpack 
    - A pre-written node module allows for the addition of sass completion to the app
*/
const rewireSass = require('react-app-rewire-scss');
module.exports = function override(config, env) {
    config = rewireSass(config, env);
    return config;
}
