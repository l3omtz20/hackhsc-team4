import { observable, action } from 'mobx';
// This store uses the MobX-utils method for ajax updates more can be found at https://github.com/mobxjs/mobx-utils#frompromise.
import { fromPromise, IPromiseBasedObservable } from 'mobx-utils';
import { IChartData, ITableData, INotificationData } from './IDashboardStore';
import { MockNotifications } from './MockData';
import ChartService from '../../services/ChartService/ChartService';
import TableService from '../../services/TableService/TableService';

export default class DashboardStore {
    // The IPromiseBasedObservable interface requires passing the interface of the the outputted data.
    @observable
    public lineChartContents: IPromiseBasedObservable<Array<IChartData>>;
    @observable
    public barChartContents: IPromiseBasedObservable<Array<IChartData>>;
    @observable
    public tableContents: IPromiseBasedObservable<Array<ITableData>>;
    @observable
    public notifications: Array<INotificationData>;
    constructor(
        // The ajax calls are made in a single service to allow multiple stores to reuse the same web api code
        private chartService: ChartService,
        private tableService: TableService
    ) {}
    // This init function allows for easily populating of all display data from one call in a view component's constructor.
    @action
    init() {
        this.getBarChartData();
        this.getLineChartData();
        this.getTableData();
        this.notifications = MockNotifications;
    }
    @action
    getLineChartData() {
        // The fromPromise function converts a promise to a PromiseBasedObservable. 
        this.lineChartContents = fromPromise( this.chartService.getLineChartData());
    }

    @action
    getBarChartData() {
        this.barChartContents = fromPromise(this.chartService.getBarChartData());
    }

    @action
    getTableData = () => {
        this.tableContents = fromPromise(this.tableService.getTableData());
    }
}