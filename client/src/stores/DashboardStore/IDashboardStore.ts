/*
    -  Here we define the dashboard's stores core data interfaces that are used to strongly type data. 
    -  Putting these in this file allow for a centralized  place that can be easily imported in various files.
*/
export interface IChartData {
    // Legend Name
    name: string;
    // Chart Data
    data: Array<number>;
    // Line color
    color: string;
}

export interface ITableData {
    // Row 'Name'
    ID: number;
    AuthDateTime: string;
    Airport: string;
    EquipmentId: string;
    EquipmentTypeID: string;
    Department: string;
    AccessStatus: string;
    Denied?: string;
    Driver: string;
}

export interface IAccelerationTableData {
    ID: number;
    acceleration_mG: number;
    threshold_mG: number;
    Location: string;
    EquipmentTypeID: string;
    EquipmentId: string;
    driver?: string;
}

export interface INotificationData {
    // whta type of notification 
    severity: 'critical' | 'important' | 'information' | 'success';
    // Test of notification
    content: string;
    // Tags to appear in notification
    tags?: Array<string>;
}
