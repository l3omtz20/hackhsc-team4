/// <reference types="jest"/>
import Transport from '@utils/Transport/Transport';
import DashboardStore from './DashboardStore';
import { MockChartData, MockTableData } from './MockData';
import ChartService from '../../services/ChartService/ChartService';
import TableService from '../../services/TableService/TableService';
describe('DashboardStore', () => {
    let instance: DashboardStore;
    const chartServiceMock = {
        getLineChartData: jest.fn().mockResolvedValue(MockChartData),
        getBarChartData: jest.fn().mockResolvedValue(MockChartData),
    } as any as ChartService;
    const tableServiceMock = {
        getTableData: jest.fn().mockResolvedValue(MockTableData),
    }  as any as TableService;

    beforeEach(() => {
        instance = new DashboardStore(chartServiceMock, tableServiceMock);
    });

    describe('Ajax Behavior', () => {
        it('has a init method that calls all the ajax method', async () => {
            instance.init();
            expect(chartServiceMock.getLineChartData).toHaveBeenCalledTimes(1);
            expect(chartServiceMock.getBarChartData).toHaveBeenCalledTimes(1);
            expect(tableServiceMock.getTableData).toHaveBeenCalledTimes(1);
        });

        it('has a getLineChartData method that populates lineChartContents', async () => {
            instance.getLineChartData();
            await instance.lineChartContents;
            expect(instance.lineChartContents.state).toBe('fulfilled');
            if (instance.lineChartContents.state === 'fulfilled') {
                expect(instance.lineChartContents.value).toEqual(MockChartData);
            }
        });

        it('has a getBarChartData method that populates barChartContents', async () => {
            instance.getBarChartData();
            await instance.barChartContents;
            expect(instance.barChartContents.state).toBe('fulfilled');
            if (instance.barChartContents.state === 'fulfilled') {
                expect(instance.barChartContents.value).toEqual(MockChartData);
            }
        });

        it('has a getTableData method that populates lineChartContents', async () => {
            instance.getTableData();
            await instance.tableContents;
            expect(instance.tableContents.state).toBe('fulfilled');
            if (instance.tableContents.state === 'fulfilled') {
                expect(instance.tableContents.value).toEqual(MockTableData);
            }
        });
    });
});