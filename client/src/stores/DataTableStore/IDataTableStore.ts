export interface ITableData { 
    ID: number;
    AuthDateTime: string;
    Airport: string;
    EquipmentId: string;
    EquipmentTypeID: string;
    Department: string;
    AccessStatus: string;
    Denied?: string;
    Driver: string;
}
