/// <reference types="jest"/>
import DataTableStore from './DataTableStore';
import { MockTableData } from '../DashboardStore/MockData';
import TableService from '../../services/TableService/TableService';
describe('DataTableStore', () => {
    let instance: DataTableStore;
   
    const mock = [{
        name: 'test',
        letter: 'a',
        index: 1
    }, {
        name: 'foo',
        letter: 'b',
        index: 2
    }];
    const tableServiceMock = {
        getTableData: jest.fn().mockResolvedValue(mock)
    }  as any as TableService;
    beforeEach(() => {
        instance = new DataTableStore(tableServiceMock);
    });

    describe('Ajax Behavior', () => {
        it('has a getTableData method that populates lineChartContents', async () => {
            instance.getTableData();
            await instance.tableContents;
            expect(instance.tableContents.state).toBe('fulfilled');
            if (instance.tableContents.state === 'fulfilled') {
                expect(instance.tableContents.value).toEqual(mock);
            }
        });
    });

    describe('Computed Behavior', () => {
        it('returns full app as displayData when no filter is set', async () => {
            instance.getTableData();
            const test = instance.displayData;
            await test;
            expect(test.state).toBe('fulfilled');
            if (test.state === 'fulfilled') {
                expect(test.value).toEqual(mock);
            }
        });

        it('returns filtered displayData when filter is set', async () => {
            instance.getTableData();
            await instance.tableContents;
            instance.setFilter(MockTableData.name);
            const test = instance.displayData;
            await test;
            expect(test.state).toBe('fulfilled');
            if (test.state === 'fulfilled') {
                expect(test.value).toEqual([MockTableData]);
            }
        });
    });
});