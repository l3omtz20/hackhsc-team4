import { observable, action, computed } from 'mobx';
import { fromPromise, IPromiseBasedObservable } from 'mobx-utils';
import { ITableData } from './IDataTableStore';
import TableService from '../../services/TableService/TableService';

export default class DataTableStore {
    @observable
    public tableContents: IPromiseBasedObservable<Array<ITableData>>;
    @observable
    public selected: Array<ITableData> = [];
    @observable
    public filterBy: string | null;
    constructor(
        private tableService: TableService
    ) {}

    @computed
    get displayData(): IPromiseBasedObservable<Array<ITableData>> {
        const filter = this.filterBy!;
        return fromPromise(this.tableContents.then((results) => {
            const data = results;
            if (filter) {
                const query = filter!.toLowerCase();
                return data.filter((item) => (item.Driver.toLowerCase().includes(query)));
            }
            else {
                return data;
            }
        }));
    }

    @action
    setFilter(query: string) {
        this.filterBy = query;
    }

    @action
    getTableData = () => {
        this.tableContents = fromPromise(this.tableService.getTableData());
    }
}