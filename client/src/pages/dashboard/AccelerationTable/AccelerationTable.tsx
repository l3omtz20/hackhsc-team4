import * as React from 'react';
import { DataTable } from '@scuf/datatable';
import ITableProps from './IAccelerationTableProps';
import { ICellData } from '@scuf/datatable/dist/components/DataTable/IDataTableInterfaces';
export default class AccelerationTable extends React.Component<ITableProps> {

    constructor(props: ITableProps) {
        super(props);
    }

    render() {
        return (
            <DataTable
                data={this.props.data}
                reorderableColumns={true}
                resizableColumns={true}
            >
                <DataTable.Column field="driver" header="Driver" sortable={true} />
                <DataTable.Column field="Location" header="Location" sortable={true} />
                <DataTable.Column field="EquipmentId" header="Equipment Id" sortable={true} />
                <DataTable.Column field="EquipmentTypeID" header="Equipment Type Id" sortable={true} />
                <DataTable.Column
                    field="acceleration_mG"
                    header="Acceleration"
                    sortable={true}
                    renderer={(cellData) => this.renderStatusCell(cellData)}
                />
                <DataTable.Column field="threshold_mG" header="Threshold" sortable={true} />
            </DataTable>
        );
    }

    private renderStatusCell = (cellData: ICellData) => {
        const color = cellData.value > cellData.rowData.threshold_mG  ? 'red' : 'green';
        return <div className="custom-cell-wrap"><DataTable.Status color={color} />{cellData.value}</div>;
    }
}