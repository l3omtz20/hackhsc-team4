import { IAccelerationTableData } from 'stores/DashboardStore/IDashboardStore';

export default interface ITableProps {
    data: Array<IAccelerationTableData>;
}