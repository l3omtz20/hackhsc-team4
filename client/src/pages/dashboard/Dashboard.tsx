import * as React from 'react';
import { Grid, Tab, Accordion, Icon } from '@scuf/common';
import { observer } from 'mobx-react';
import ListHeader from '../../partials/ListHeader/ListHeader';
import  Checkbox  from 'semantic-ui-react/dist/commonjs/modules/Checkbox/Checkbox';

import './Dashboard.scss';
import Table from './Table/Table';
import AccelerationTable from './AccelerationTable/AccelerationTable';
import IDashboardProps from './IDashboardProps';
const equipment = [{'ID':   1,  'AuthDateTime':   '2018-11-01T02:  24:  00.000Z',  'Airport':   'LHR',  'EquipmentId':  'PIN01210',  'EquipmentTypeID':  'UV',  'Department':  'DEP20',  'AccessStatus':  'Denied',  'Driver':  'Harold Odonoghue'},  {'ID':  2,  'AuthDateTime':  '2018-11-01T02:  24:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00689',  'EquipmentTypeID':  'TEL',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Therese Hoover'},  {'ID':  3,  'AuthDateTime':  '2018-11-01T02:  24:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01210',  'EquipmentTypeID':  'UV',  'Department':  'DEP20',  'AccessStatus':  'Denied',  'Driver':  'Harold Odonoghue'},  {'ID':  4,  'AuthDateTime':  '2018-11-01T02:  24:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01210',  'EquipmentTypeID':  'UV',  'Department':  'DEP20',  'AccessStatus':  'Denied',  'Driver':  'Harold Odonoghue'},  {'ID':  5,  'AuthDateTime':  '2018-11-01T02:  23:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01210',  'EquipmentTypeID':  'UV',  'Department':  'DEP20',  'AccessStatus':  'Denied',  'Driver':  'Harold Odonoghue'},  {'ID':  7,  'AuthDateTime':  '2018-11-01T02:  23:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00641',  'EquipmentTypeID':  'SET',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Natasha Stevens'},  {'ID':  8,  'AuthDateTime':  '2018-11-01T02:  23:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00649',  'EquipmentTypeID':  'SET',  'Department':  'DEP5',  'AccessStatus':  'Authorised',  'Driver':  'Rogelio Warren'},  {'ID':  10,  'AuthDateTime':  '2018-11-01T02:  21:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00659',  'EquipmentTypeID':  'SET',  'Department':  'DEP5',  'AccessStatus':  'Authorised',  'Driver':  'Rogelio Warren'},  {'ID':  11,  'AuthDateTime':  '2018-11-01T02:  21:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01435',  'EquipmentTypeID':  'BC',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Justin Chaney'},  {'ID':  12,  'AuthDateTime':  '2018-11-01T02:  21:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01390',  'EquipmentTypeID':  'TRMC',  'Department':  'DEP10',  'AccessStatus':  'Authorised',  'Driver':  'Maria Dawe'},  {'ID':  13,  'AuthDateTime':  '2018-11-01T02:  19:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00936',  'EquipmentTypeID':  'LDG',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Craig Kelley'},  {'ID':  14,  'AuthDateTime':  '2018-11-01T02:  19:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00363',  'EquipmentTypeID':  'TT',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Helen Frantz'},  {'ID':  15,  'AuthDateTime':  '2018-11-01T02:  19:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01163',  'EquipmentTypeID':  'ACM',  'Department':  'DEP6',  'AccessStatus':  'Authorised',  'Driver':  'Maxine Dickey'},  {'ID':  16,  'AuthDateTime':  '2018-11-01T02:  18:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01421',  'EquipmentTypeID':  'BC',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Gene Tapper'},  {'ID':  17,  'AuthDateTime':  '2018-11-01T02:  17:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01175',  'EquipmentTypeID':  '400',  'Department':  'DEP6',  'AccessStatus':  'Authorised',  'Driver':  'Greg Taylor'},  {'ID':  18,  'AuthDateTime':  '2018-11-01T02:  17:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00650',  'EquipmentTypeID':  'SET',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Eric Kitchen'},  {'ID':  19,  'AuthDateTime':  '2018-11-01T02:  17:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01418',  'EquipmentTypeID':  'BC',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Jeremy Banister'},  {'ID':  20,  'AuthDateTime':  '2018-11-01T02:  17:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00477',  'EquipmentTypeID':  'BC',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Helen Frantz'},  {'ID':  21,  'AuthDateTime':  '2018-11-01T02:  17:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00641',  'EquipmentTypeID':  'SET',  'Department':  'DEP5',  'AccessStatus':  'Authorised',  'Driver':  'Rogelio Warren'},  {'ID':  22,  'AuthDateTime':  '2018-11-01T02:  16:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00127',  'EquipmentTypeID':  'SETO',  'Department':  'DEP5',  'AccessStatus':  'Authorised',  'Driver':  'Kathleen Thoma'},  {'ID':  23,  'AuthDateTime':  '2018-11-01T02:  15:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01390',  'EquipmentTypeID':  'TRMC',  'Department':  'DEP10',  'AccessStatus':  'Authorised',  'Driver':  'Maria Dawe'},  {'ID':  24,  'AuthDateTime':  '2018-11-01T02:  15:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01004',  'EquipmentTypeID':  '280',  'Department':  'DEP6',  'AccessStatus':  'Authorised',  'Driver':  'Johanna Thornton'},  {'ID':  25,  'AuthDateTime':  '2018-11-01T02:  14:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00909',  'EquipmentTypeID':  'LDG',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Edwin Petersen'},  {'ID':  26,  'AuthDateTime':  '2018-11-01T02:  14:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00204',  'EquipmentTypeID':  'SET',  'Department':  'DEP5',  'AccessStatus':  'Authorised',  'Driver':  'Rogelio Warren'},  {'ID':  27,  'AuthDateTime':  '2018-11-01T02:  13:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00942',  'EquipmentTypeID':  'TRMC',  'Department':  'DEP10',  'AccessStatus':  'Authorised',  'Driver':  'Rick Harris'},  {'ID':  28,  'AuthDateTime':  '2018-11-01T03:  59:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN01390',  'EquipmentTypeID':  'TRMC',  'Department':  'DEP10',  'AccessStatus':  'Authorised',  'Driver':  'Maria Dawe'},  {'ID':  29,  'AuthDateTime':  '2018-11-01T03:  57:  00.000Z',  'Airport':  'LHR',  'EquipmentId':  'PIN00375',  'EquipmentTypeID':  'TT',  'Department':  'DEP9',  'AccessStatus':  'Authorised',  'Driver':  'Karen Sturgis'}];
const acceleration = [{'ID': 1, 'DateTime': '2018-11-01T02: 15: 04.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 455, 'threshold_mG': 400, 'Location': 'T5B', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 2, 'DateTime': '2018-11-01T02: 13: 58.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 425, 'threshold_mG': 400, 'Location': 'T5C', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 3, 'DateTime': '2018-11-01T01: 54: 59.000Z', 'driver': 'Bobby Trimm', 'acceleration_mG': 448, 'threshold_mG': 400, 'Location': 'T5A Northern Tunnel', 'EquipmentId': 'PIN00949', 'EquipmentTypeID': 'TRMC'}, {'ID': 4, 'DateTime': '2018-11-01T01: 26: 39.000Z', 'driver': '-', 'acceleration_mG': 400, 'threshold_mG': 400, 'Location': '-', 'EquipmentId': 'PIN00943', 'EquipmentTypeID': 'TRMC'}, {'ID': 5, 'DateTime': '2018-11-01T01: 26: 01.000Z', 'driver': '-', 'acceleration_mG': 417, 'threshold_mG': 400, 'Location': 'T5D Tunnel', 'EquipmentId': 'PIN00943', 'EquipmentTypeID': 'TRMC'}, {'ID': 6, 'DateTime': '2018-11-01T00: 48: 37.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 479, 'threshold_mG': 400, 'Location': 'T5C', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 7, 'DateTime': '2018-11-01T00: 46: 49.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 413, 'threshold_mG': 400, 'Location': 'T5 Southern Tunnel', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 8, 'DateTime': '2018-11-01T00: 45: 40.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 413, 'threshold_mG': 400, 'Location': 'T5B', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 9, 'DateTime': '2018-11-01T00: 13: 34.000Z', 'driver': 'Esteban Brown', 'acceleration_mG': 312, 'threshold_mG': 300, 'Location': '561', 'EquipmentId': 'PIN01199', 'EquipmentTypeID': '280'}, {'ID': 10, 'DateTime': '2018-10-31T22: 39: 05.000Z', 'driver': 'Esteban Brown', 'acceleration_mG': 319, 'threshold_mG': 300, 'Location': 'T5C', 'EquipmentId': 'PIN01199', 'EquipmentTypeID': '280'}, {'ID': 11, 'DateTime': '2018-10-31T22: 38: 59.000Z', 'driver': 'Esteban Brown', 'acceleration_mG': 300, 'threshold_mG': 300, 'Location': '-', 'EquipmentId': 'PIN01199', 'EquipmentTypeID': '280'}, {'ID': 12, 'DateTime': '2018-10-31T22: 37: 22.000Z', 'driver': 'Hettie Willis', 'acceleration_mG': 303, 'threshold_mG': 300, 'Location': 'T5C', 'EquipmentId': 'PIN01018', 'EquipmentTypeID': '280'}, {'ID': 13, 'DateTime': '2018-10-31T22: 10: 43.000Z', 'driver': 'Greg Taylor', 'acceleration_mG': 303, 'threshold_mG': 300, 'Location': '562', 'EquipmentId': 'PIN01203', 'EquipmentTypeID': '280'}, {'ID': 14, 'DateTime': '2018-10-31T22: 09: 52.000Z', 'driver': 'Bobby Trimm', 'acceleration_mG': 434, 'threshold_mG': 400, 'Location': 'T5B Southern Tunnel', 'EquipmentId': 'PIN00949', 'EquipmentTypeID': 'TRMC'}, {'ID': 15, 'DateTime': '2018-10-31T20: 42: 22.000Z', 'driver': 'Esteban Brown', 'acceleration_mG': 334, 'threshold_mG': 300, 'Location': '-', 'EquipmentId': 'PIN01199', 'EquipmentTypeID': '280'}, {'ID': 16, 'DateTime': '2018-10-31T19: 57: 56.000Z', 'driver': 'William Snyder', 'acceleration_mG': 526, 'threshold_mG': 400, 'Location': 'T5A Northern Tunnel', 'EquipmentId': 'PIN00908', 'EquipmentTypeID': 'TRMC'}, {'ID': 17, 'DateTime': '2018-10-31T19: 53: 49.000Z', 'driver': 'William Snyder', 'acceleration_mG': 443, 'threshold_mG': 400, 'Location': '-', 'EquipmentId': 'PIN00908', 'EquipmentTypeID': 'TRMC'}, {'ID': 18, 'DateTime': '2018-10-31T19: 49: 53.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 493, 'threshold_mG': 400, 'Location': 'T5C', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 19, 'DateTime': '2018-10-31T19: 49: 23.000Z', 'driver': 'Sandra Rivera', 'acceleration_mG': 319, 'threshold_mG': 300, 'Location': '-', 'EquipmentId': 'PIN01090', 'EquipmentTypeID': '280'}, {'ID': 20, 'DateTime': '2018-10-31T19: 49: 11.000Z', 'driver': 'Sandra Rivera', 'acceleration_mG': 300, 'threshold_mG': 300, 'Location': '-', 'EquipmentId': 'PIN01090', 'EquipmentTypeID': '280'}, {'ID': 21, 'DateTime': '2018-10-31T18: 39: 49.000Z', 'driver': 'Hettie Willis', 'acceleration_mG': 303, 'threshold_mG': 300, 'Location': 'T5C', 'EquipmentId': 'PIN01018', 'EquipmentTypeID': '280'}, {'ID': 22, 'DateTime': '2018-10-31T18: 19: 20.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 415, 'threshold_mG': 400, 'Location': 'T5B', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 23, 'DateTime': '2018-10-31T18: 19: 05.000Z', 'driver': 'Rick Harris', 'acceleration_mG': 437, 'threshold_mG': 400, 'Location': '-', 'EquipmentId': 'PIN00942', 'EquipmentTypeID': 'TRMC'}, {'ID': 24, 'DateTime': '2018-10-31T18: 16: 50.000Z', 'driver': 'Hettie Willis', 'acceleration_mG': 306, 'threshold_mG': 300, 'Location': 'T5C', 'EquipmentId': 'PIN01018', 'EquipmentTypeID': '280'}, {'ID': 25, 'DateTime': '2018-10-31T18: 16: 44.000Z', 'driver': 'Hettie Willis', 'acceleration_mG': 336, 'threshold_mG': 300, 'Location': 'T5C', 'EquipmentId': 'PIN01018', 'EquipmentTypeID': '280'}, {'ID': 26, 'DateTime': '2018-10-31T18: 03: 02.000Z', 'driver': 'Hettie Willis', 'acceleration_mG': 385, 'threshold_mG': 300, 'Location': 'T5 Southern Tunnel', 'EquipmentId': 'PIN01018', 'EquipmentTypeID': '280'}, {'ID': 27, 'DateTime': '2018-10-31T17: 39: 19.000Z', 'driver': 'David Stokes', 'acceleration_mG': 436, 'threshold_mG': 400, 'Location': 'T5 Northern Tunnel', 'EquipmentId': 'PIN00949', 'EquipmentTypeID': 'TRMC'}, {'ID': 28, 'DateTime': '2018-10-31T17: 39: 02.000Z', 'driver': 'David Stokes', 'acceleration_mG': 438, 'threshold_mG': 400, 'Location': 'T5 Northern Tunnel', 'EquipmentId': 'PIN00949', 'EquipmentTypeID': 'TRMC'}, {'ID': 29, 'DateTime': '2018-10-31T17: 02: 27.000Z', 'driver': 'Sandra Rivera', 'acceleration_mG': 380, 'threshold_mG': 300, 'Location': '-', 'EquipmentId': 'PIN01090', 'EquipmentTypeID': '280'}];
@observer
export class DashboardPage extends React.Component<IDashboardProps> {

    render() {
        return (
            <Grid>
                <ListHeader
                    title="Heathrow Airport"
                    description=""
                />
                <Grid.Row>
                    <Grid.Column width={12}>
                        <Tab defaultActiveIndex={1} className="plan-tab"> 
                            <Tab.Pane title="Equipment management">
                                <Table data={equipment}/>
                            </Tab.Pane>
                            <Tab.Pane title="Acceleration management">
                                <AccelerationTable data={acceleration}/>
                            </Tab.Pane>
                            <Tab.Pane title="Map of Scheduled flights">
                                <div className="map-container">
                                    <Grid.Row>
                                        <Grid.Column width={4}>
                                            <Accordion>
                                                <Accordion.Content title="Turnaround Status" active={true}>
                                                    <div>
                                                        <Checkbox label="On Schedule" /> 
                                                        <Icon root="aero" name="ac-right" size="medium" flipped="horizontally" className="plane-green"/>
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Needs Help" />
                                                        <Icon root="aero" name="ac-right" size="medium" flipped="horizontally" className="plane-orange"/>
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Major Issues" />
                                                        <Icon root="aero" name="ac-right" size="medium" flipped="horizontally" className="plane-red"/>
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Upcoming Turnaround" />
                                                        <Icon root="aero" name="ac-right" size="medium" flipped="horizontally" className="plane-blue"/>
                                                    </div>
                                                </Accordion.Content>
                                                <Accordion.Content title="Employees" active={true}>
                                                    <div>
                                                        <Checkbox label="Baggage Employees" />
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Catering Employees" />
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Fueling Employees" />
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Turn-aroung manager" />
                                                    </div>
                                                </Accordion.Content>
                                                <Accordion.Content title="Vehicles" active={true}>
                                                    <div>
                                                        <Checkbox label="Baggage Vehicles" />
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Catering Vehicles" />
                                                    </div>
                                                    <div>
                                                        <Checkbox label="Fueling Vehicles" />
                                                    </div>
                                                </Accordion.Content>
                                            </Accordion>
                                        </Grid.Column>
                                    </Grid.Row>
                                </div>
                            </Tab.Pane>
                        </Tab>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default DashboardPage;