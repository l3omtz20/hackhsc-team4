import * as React from 'react';
import { DataTable } from '@scuf/datatable';
import ITableProps from './ITableProps';
import { ICellData } from '@scuf/datatable/dist/components/DataTable/IDataTableInterfaces';
export default class Table extends React.Component<ITableProps> {

    constructor(props: ITableProps) {
        super(props);
    }

    render() {
        return (
            <DataTable
                data={this.props.data}
                reorderableColumns={true}
                resizableColumns={true}
            >
                <DataTable.Column field="Airport" header="Airport" sortable={true} />
                <DataTable.Column field="EquipmentId" header="Equipment Id" sortable={true} />
                <DataTable.Column field="Department" header="Department" sortable={true} />
                <DataTable.Column field="Driver" header="Driver" sortable={true} />
                <DataTable.Column
                    field="AccessStatus"
                    header="Access Status"
                    sortable={true}
                    renderer={(cellData) => this.renderStatusCell(cellData)}
                />
            </DataTable>
        );
    }

     private renderStatusCell = (cellData: ICellData) => {
        const color = cellData.value === 'Denied' ? 'red' : 'green';
        return <div className="custom-cell-wrap"><DataTable.Status color={color} />{cellData.value}</div>;
    }
}