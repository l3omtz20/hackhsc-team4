import { ITableData } from 'stores/DashboardStore/IDashboardStore';

export default interface ITableProps {
    data: Array<ITableData>;
}