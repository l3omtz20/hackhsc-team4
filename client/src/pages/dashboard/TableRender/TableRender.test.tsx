/// <reference types="jest"/>
import * as React from 'react';
import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import  TableRender from './TableRender';
import { Table } from '@scuf/common';
configure({ adapter: new Adapter() });

describe('<TableRender />', () => {
    let results = [{ name: 'test one', letter: 'a', index: 1 }, { name: 'test two', letter: 'b', index: 2 }];
    it('renders without crashing', () => {
        shallow(<TableRender data={results}/>);
    });

    it('renders the right amount of cells', () => {
        const wrapper =  shallow(<TableRender data={results}/>);
        expect(wrapper.find(Table.Cell)).toHaveLength(6);
    });
});