import DashboardStore from '../../stores/DashboardStore/DashboardStore';
import DataTableStore from '../../stores/DataTableStore/DataTableStore';

export default interface IDashboardProps { 
    dashboardStore?: DashboardStore;
    dataTableStore?: DataTableStore;
}