/// <reference types="jest"/>
import stores from './stores';
import Transport from './utils/Transport/Transport';
import MockAuthProvider from './utils/Transport/MockAuthProvider';
import DashboardStore from './stores/DashboardStore/DashboardStore';
import { GlobalRouterStore } from './stores/RouterStore';
import HeroStore from './stores/HeroStore/HeroStore';
import GlobalStore from './stores/GlobalStore/GlobalStore';
import DataTableStore from './stores/DataTableStore/DataTableStore';
import FormStore from './stores/FormStore/FormStore';

describe('Stores.ts', () => {
    it('has a routerStore key that is an instance of RouterStore', () => {
        expect(stores.routerStore).toBeInstanceOf(GlobalRouterStore);
    });
    it('has a dashboardStore key that is an is an instance of DashboardStore', () => {
        expect(stores.dashboardStore).toBeInstanceOf(DashboardStore);
    });
    it('has a dataTableStore key that is an is an instance of DataTableStore', () => {
        expect(stores.dataTableStore).toBeInstanceOf(DataTableStore);
    });
    it('has a heroStore key that is an is an instance of HeroStore', () => {
        expect(stores.heroStore).toBeInstanceOf(HeroStore);
    });
    it('has a globalStore key that is an is an instance of GlobalStore', () => {
        expect(stores.globalStore).toBeInstanceOf(GlobalStore);
    });
    it('has a formStore key that is an is an instance of FormStore', () => {
        expect(stores.formStore).toBeInstanceOf(FormStore);
    });
});