
import Transport from './utils/Transport/Transport';
import MockAuthProvider from './utils/Transport/MockAuthProvider';
import DashboardStore from './stores/DashboardStore/DashboardStore';
import { GlobalRouterStore } from './stores/RouterStore';
import GlobalStore from './stores/GlobalStore/GlobalStore';
import DataTableStore from './stores/DataTableStore/DataTableStore';
import ChartService from './services/ChartService/ChartService';
import TableService from './services/TableService/TableService';

const auth = new MockAuthProvider();
const transport = new Transport(auth);
const tableService = new TableService(transport);
const chartService = new ChartService(transport);
const table = new DataTableStore(tableService);

export default {
    routerStore: new GlobalRouterStore(),
    dashboardStore: new DashboardStore(chartService, tableService),
    dataTableStore: table,
    globalStore: new GlobalStore()
};