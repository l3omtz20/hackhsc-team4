/// <reference types="jest"/>
import * as React from 'react';
import * as Adapter from 'enzyme-adapter-react-16';
import { AppHeader } from './Header';
import { GlobalRouterStore } from '../../stores/RouterStore';
import { configure, shallow } from 'enzyme';
import GlobalStore from '../../stores/GlobalStore/GlobalStore';

configure({ adapter: new Adapter() });

describe('<AppHeader />', () => {
    let globalStore: GlobalStore;
    let routingStore:  GlobalRouterStore;

    beforeEach(() => {
        routingStore = { navigate: jest.fn(), route: {params: { deviceId: 'deviceId1', projectId: 'test' }} } as any as GlobalRouterStore;
        globalStore = {
            toggleSideBar: jest.fn()
        } as any as GlobalStore;

    });

    it('renders without crashing', () => {
        shallow(<AppHeader routerStore={routingStore} globalStore={globalStore}/>);
    });

    it('toggles menu close', () => {
        const wrapper =  shallow(<AppHeader routerStore={routingStore} globalStore={globalStore}/>);
        wrapper.prop('onMenuToggle')(false);
    });

    it('toggles menu open', () => {
        const wrapper =  shallow(<AppHeader routerStore={routingStore} globalStore={globalStore}/>);
        wrapper.prop('onMenuToggle')(true);
    });

    it('closes the menu when the onHeaderTransition sets collapsed to true', () => {
        const wrapper =  shallow(<AppHeader routerStore={routingStore} globalStore={globalStore}/>);
        wrapper.prop('onHeaderTransition')({ compressed: true, collapsed: true });
        expect(globalStore.toggleSideBar).toHaveBeenCalledTimes(1);
        expect(globalStore.toggleSideBar).toHaveBeenCalledWith(true);
    });
});