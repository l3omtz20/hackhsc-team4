import GlobalStore from '../../stores/GlobalStore/GlobalStore';
export interface IHeaderProps {
    globalStore?: GlobalStore;
}