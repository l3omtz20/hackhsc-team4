/// <reference types="jest"/>
import * as React from 'react';
import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import AppFooter from './Footer';
configure({ adapter: new Adapter() });
describe('<AppFooter />', () => {
    it('renders without crashing', () => {
        shallow(<AppFooter />);
    });
});