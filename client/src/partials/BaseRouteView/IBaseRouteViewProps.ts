import { GlobalRouterStore } from '../../stores/RouterStore';
export default interface IBaseRouteViewProps {
    route?: GlobalRouterStore;
}