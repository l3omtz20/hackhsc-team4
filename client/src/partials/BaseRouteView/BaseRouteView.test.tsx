import * as React from 'react';
import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { BaseRouteView } from './BaseRouteView';
import { RouteView } from 'react-mobx-router5';
import GlobalRouterStore from '../../stores/RouterStore';
configure({ adapter: new Adapter() });

describe('<BaseRouteView />', () => {
    it('renders without crashing', () => {
        shallow(<BaseRouteView route={GlobalRouterStore} />);
    });

    it('renders a RouteView', () => {
        const wrapper = shallow(<BaseRouteView route={GlobalRouterStore}/>);
        expect(wrapper.is(RouteView)).toBeTruthy();
    });
});