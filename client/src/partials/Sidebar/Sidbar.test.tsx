/// <reference types="jest"/>
import * as React from 'react';
import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { RouterStore } from 'mobx-react-router';
import { AppSidebar } from './Sidebar';
import { SidebarLayout } from '@scuf/common';
import Routes from '@routes';
configure({ adapter: new Adapter() });

describe('<AppSidebar />', () => {
    const routingStore = { navigate: jest.fn(), route: { name: 'dashboard' } } as any as GlobalRouterStore;
    const globalMock = { sideBarCollapsed: true } as any as GlobalStore;
    beforeEach(() => {
        (routingStore.navigate as jest.Mock).mockReset();
    });

    it('renders without crashing', () => {
        shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
    });

    describe('markdown', () => {
        it('is wrapped in SidebarLayout', () => {
            const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
            expect(wrapper.is(SidebarLayout)).toBeTruthy();
        });

        describe('handles onClick to navigate for each item', () => {
            it('naviagtes to dashboard', () => {
                const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
                const sidebar = wrapper.find(SidebarLayout.Sidebar);
                sidebar.childAt(0).prop('onClick')();
                expect(routingStore.navigate).toHaveBeenCalled();
                expect(routingStore.navigate).toHaveBeenCalledWith('dashboard');
            });

            it('naviagtes to datatable', () => {
                const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
                const sidebar = wrapper.find(SidebarLayout.Sidebar);
                sidebar.childAt(1).prop('onClick')();
                expect(routingStore.navigate).toHaveBeenCalled();
                expect(routingStore.navigate).toHaveBeenCalledWith('datatable');
            });
            it('naviagtes to hero', () => {
                const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
                const sidebar = wrapper.find(SidebarLayout.Sidebar);
                sidebar.childAt(2).prop('onClick')();
                expect(routingStore.navigate).toHaveBeenCalled();
                expect(routingStore.navigate).toHaveBeenCalledWith('hero');
            });
            it('naviagtes to form', () => {
                const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
                const sidebar = wrapper.find(SidebarLayout.Sidebar);
                sidebar.childAt(3).prop('onClick')();
                expect(routingStore.navigate).toHaveBeenCalled();
                expect(routingStore.navigate).toHaveBeenCalledWith('form');
            });
            it('naviagtes to routing', () => {
                const wrapper = shallow(<AppSidebar routerStore={routingStore} globalStore={globalMock} />);
                const sidebar = wrapper.find(SidebarLayout.Sidebar);
                sidebar.childAt(4).prop('onClick')();
                expect(routingStore.navigate).toHaveBeenCalled();
                expect(routingStore.navigate).toHaveBeenCalledWith('routing');
            });
        });
    });
});