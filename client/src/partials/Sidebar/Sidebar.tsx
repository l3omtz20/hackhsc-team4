import * as React from 'react';
import { SidebarLayout } from '@scuf/common';
import { observer, inject } from 'mobx-react';
import ISidebarProps from './ISidebarProps';
import './Sidebar.scss';

// The const allows for shortening names of view subcomponents
const Sidebar = SidebarLayout.Sidebar;
@observer
export class AppSidebar extends React.Component<ISidebarProps> {
    render() {
        const globalStore = this.props.globalStore!;
        // Here the global router store is used to track the current routes name, this is used for active link detection
        const { name } = this.props.routerStore!.route;
        // Navigation works through the routerStore's navigate function
        const router = this.props.routerStore!;

        return (
            <SidebarLayout collapsed={globalStore.sideBarCollapsed} className="app-sidebar">
                <Sidebar>
                    <Sidebar.Item
                        content="Dashboard Page"
                        icon="dashboard"
                        iconRoot="building"
                        onClick={() => router.navigate('dashboard')}
                        active={name === 'dashboard'}
                    />
                    {/* <Sidebar.Item
                        content="DataTable Page"
                        icon="multiple-devices"
                        iconRoot="building"
                        onClick={() => router.navigate('datatable')}
                        active={name === 'datatable'}
                    /> */}
                </Sidebar>
                <SidebarLayout.Content>
                    {this.props.children}
                </SidebarLayout.Content>
            </SidebarLayout>
        );
    }
}
export default inject('routerStore', 'globalStore')(AppSidebar);