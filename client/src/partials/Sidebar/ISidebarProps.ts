import { RouterStore } from 'mobx-router5';
import GlobalStore from '../../stores/GlobalStore/GlobalStore';
 
export default interface ISidebarProps {
    routerStore?: RouterStore;
    globalStore?: GlobalStore;
}