import { observable, action, computed, observe } from 'mobx';
import { single } from 'validate.js';
import { FormConfigData, FormDirty, PatchFormData, FormObject } from '@utils/ValidationService/IValdation';
export default class ValidationService<T extends FormObject> {
    @observable
    public value: T;
    @observable
    private formDirty: FormDirty = {};
    @observable
    private formData: PatchFormData = {};
    private validationDictionary = {
        required: {
            presence: { message: 'Required', allowEmpty: false }
        },
        https: {
            url: { message: 'Invalid url. Must be https://', schemes: ['https'] }
        },
        guid: {
            format: { message: 'Not a valid GUID', pattern: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/, flags: 'i' }
        },
        noSpaces: {
            format: { message: 'No Spaces Allowed', pattern: /^\S*$/ }
        }
    };

    constructor(private config: FormConfigData<T>) {
        const value = {} as T;

        for (let key in this.config) {
            const initialValue = this.config[key].value ? this.config[key].value : '';
            this.formData[key] = initialValue;
            value[key] = initialValue;
            this.formDirty[key] = false;
        }

        this.value = value;

        observe(this.value, (val) => {
            this.formDirty[val.name] = true;
        });
    }

    @action
    setFormValue(data: T) {
        for (let key in this.config) {
            this.config[key].value = data[key];
            this.value[key] = data[key];
            this.formData[key] = data[key];
            this.formDirty[key] = false;
        }
    }

    @action
    resetForm() {
        this.formDirty = {};
        for (let key in this.config) {
            this.config[key].value = this.formData[key];
            this.value[key] = this.formData[key] ? this.formData[key] : '';
            this.formDirty[key] = false;
        }
    }

    @action
    clearForm() {
        this.formData = {};
        this.formDirty = {};
        for (let key in this.config) {
            this.config[key].value = undefined;
            this.value[key] = '';
            this.formDirty[key] = false;
        }
    }

    @action
    resetValue(key: string) {
        if (this.config.hasOwnProperty(key)) {
            if (this.config[key] && this.formData[key]) {
                this.value[key] = this.formData[key];
            }
            else {
                this.value[key] = undefined;
            }
            this.formDirty[key] = false;
        }
    }

    @action
    clearValue(key: string) {
        if (this.config.hasOwnProperty(key)) {
            this.formDirty[key] = false;
            this.value[key] = undefined;
            this.config[key].value = undefined;
        }
    }

    @computed
    get dirty() {
        const dirtyOutput: {
            [P in keyof T]?: boolean
        } = {};
        for (let key in this.formDirty) {
            dirtyOutput[key] = this.formDirty[key];
        }
        return dirtyOutput;
    }

    @computed
    get formIsDirty() {
        for (let key in this.formDirty) {
            if (this.formDirty[key]) {
                return true;
            }
        }
        return false;
    }

    @computed
    get formIsClean() {
        for (let key in this.formDirty) {
            if (this.formDirty[key]) {
                return false;
            }
        }
        return true;
    }

    @computed
    get formIsValid() {
        return Object.keys(this.errors).length === 0;
    }

    @computed
    get formIsInvalid() {
        return !this.formIsValid;
    }

    @computed
    get errors() {
        const errorOutput: {
            [P in keyof T]?: string
        } = {};
        for (let key in this.config) {
            if (this.config[key].validators && this.config[key].validators!.length) {
                const validators = this.config[key].validators!;
                const reset = !this.value[key] && validators.indexOf('required') === -1;
                validators.forEach((validatorKey) => {
                    if (this.validationDictionary.hasOwnProperty(validatorKey)) {
                        let errors = single(this.value[key], this.validationDictionary[validatorKey]);
                        if (errors && !reset) {
                            errorOutput[key] = errors[0];
                            return true;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                });
            }
        }
        return errorOutput;
    }

    @computed
    get dirtyErrors() {
        const errorOutput: {
            [P in keyof T]?: string
        } = {};
        for (let key in this.errors) {
            if (this.formDirty[key]) {
                errorOutput[key] = this.errors[key];
            }
        }
        return errorOutput;
    }
    
    @action
    setValidator(name: string, rule: object) {
       this.validationDictionary[name] = rule;
    }
}