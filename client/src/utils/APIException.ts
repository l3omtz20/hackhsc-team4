
export default class APIException extends Error {
    public readonly statusCode: number;
    public readonly status: string;
    
    constructor(public message: string, public res: Response) {
        super(message);
        this.statusCode = res.status;
        this.status = res.statusText;
    }
}