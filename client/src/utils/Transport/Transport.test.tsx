import Transport from './Transport';
import MockAuth from './MockAuthProvider';
import * as Mock from 'jest-fetch-mock';
declare let global: { fetch: Mock };

describe('Transport Class', () => {
    let instance: Transport;
    const auth = new MockAuth();
    let mockResponseInit;
    let testResponseInit;
    global.fetch = Mock;
    auth.login = jest.fn();
    const headers = { 'map': { 'accept': 'application/json', 'authorization': 'Bearer mock-token', 'content-type': 'application/json' } };
    beforeEach(() => {
        mockResponseInit = { 'headers': { 'content-type': 'application/json' }, status: 200};
        testResponseInit = { 'headers': { 'map': { 'accept': 'application/json',  'authorization': 'Bearer mock-token', 'content-type': 'application/json' }}};
        instance = new Transport(auth);
    });

    afterEach(() => {
        global.fetch.resetMocks();
    });

    describe('Get Method', () => {
        it('Basic Get method calls fetch with the right url', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            instance.get('test.com');
            expect(global.fetch).toHaveBeenCalledTimes(1);
            testResponseInit.method = 'GET';
            expect(global.fetch).toHaveBeenCalledWith('test.com', testResponseInit);
        });

        it('Get method with params calls the right url', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            instance.get('test.com', { foo: 'bar', bool: 'true' });
            expect(global.fetch).toHaveBeenCalledTimes(1);
            testResponseInit.method = 'GET';
            expect(global.fetch).toHaveBeenCalledWith('test.com?foo=bar&bool=true', testResponseInit);
        });

        it('Success Get method returns promise with data', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            let test = instance.get('test.com', { foo: 'bar', bool: 'true' });
            expect(test).toBeInstanceOf(Promise);
            return expect(test).resolves.toEqual(({ data: '12345' }));
        });

        it('Success Get method returns promise with data with application/zip', () => {
            const mockResponseInitZip = { 'headers': { 'content-type': 'application/zip' }, status: 200};
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInitZip);
            let test = instance.get('test.com', { foo: 'bar', bool: 'true' });
            expect(test).toBeInstanceOf(Promise);
            expect(test).resolves.toEqual(new Blob([JSON.stringify({ data: '12345' })]));
        });

        it('append authorization header', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
            instance.get('test.com', { foo: 'bar', bool: 'true' });
            expect(global.fetch).toHaveBeenCalledTimes(1);
            testResponseInit.method = 'GET';
            expect(global.fetch).toHaveBeenCalledWith('test.com?foo=bar&bool=true', testResponseInit);

        });

        it('Error on Get method returns promise with APIException', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 400, 'statusText': 'No Auth' });
            let test = instance.get('test.com', { foo: 'bar', bool: 'true' });
            const prom = Promise.reject({status: 'test'});
            expect(test).rejects.toBeInstanceOf(Promise.reject);
            await expect(test).rejects.toHaveProperty('status', 400);
            await expect(test).rejects.toHaveProperty('statusText', 'No Auth');
        });

        it('401 Error after successful login throws exception', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 401, 'statusText': 'No Auth' });
            const results = instance.get('test.com', { foo: 'bar', bool: 'true' });
            expect(results).rejects.toBeInstanceOf(Promise.reject);
        });
    });

    describe('Delete Method', () => {
        it('Basic Delete method calls fetch with the right url', () => {
            global.fetch.mockResponseOnce('', { 'status': 200});
            instance.delete('test.com');
            expect(global.fetch).toHaveBeenCalledTimes(1);
            const headers = new Headers();
            testResponseInit.method = 'DELETE';
            expect(global.fetch).toHaveBeenCalledWith('test.com', testResponseInit);
        });

        it('Delete method with params calls the right url', () => {
            global.fetch.mockResponseOnce('', { 'status': 200});
            instance.delete('test.com', { foo: 'bar', bool: 'true' });
            expect(global.fetch).toHaveBeenCalledTimes(1);
            testResponseInit.method = 'DELETE';
            expect(global.fetch).toHaveBeenCalledWith('test.com?foo=bar&bool=true', testResponseInit);
        });
    });

    describe('POST Method', () => {
        it('Basic POST method calls fetch with the right url and empty', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
            instance.post('test.com', {});
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': '{}', 'headers': headers, 'method': 'POST' });
        });

        it('Basic POST method calls fetch with the right url and body', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
            const body = { test: 'test' };
            instance.post('test.com', body);
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': JSON.stringify(body), 'headers': headers, 'method': 'POST' });
        });


        it('Basic POST method calls fetch with the right url and body and access token - append auth header', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            const body = { test: 'test' };
            instance.post('test.com', body);
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': JSON.stringify(body), 'headers': headers, 'method': 'POST' });
        });

        it('Success POST method returns promise with data', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            const body = { test: 'test' };
            let test = instance.post('test.com', body);
            expect(test).toBeInstanceOf(Promise);
            return expect(test).resolves.toEqual(({ data: '12345' }));
        });

        it('Error on POST method returns promise with APIException', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 400, 'statusText': 'No Auth' });
            let test = instance.post('test.com', {});

            expect(test).rejects.toBeInstanceOf(Promise.reject);
            await expect(test).rejects.toHaveProperty('status', 400);
            await expect(test).rejects.toHaveProperty('statusText', 'No Auth');
        });

        it('401 Error on POST method returns promise with APIException', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 401, 'statusText': 'No Auth' });
            let test = instance.post('test.com', {});
            await expect(test).toBeInstanceOf(Promise);
            expect(auth.login).toBeCalled();
        });
    });

    describe('PUT Method', () => {
        it('Basic PUT method calls fetch with the right url and empty', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            instance.put('test.com', {});
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': '{}', 'headers':headers, 'method': 'PUT' });
        });

        it('Basic PUT method calls fetch with the right url and body', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            const body = { test: 'test' };
            instance.put('test.com', body);
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': JSON.stringify(body), 'headers': headers, 'method': 'PUT' });
        });

        it('Basic PUT method calls fetch with the right url and body and access token - append auth header', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            const body = { test: 'test' };
            instance.put('test.com', body);
            expect(global.fetch).toHaveBeenCalledTimes(1);
            expect(global.fetch).toHaveBeenCalledWith('test.com', { 'body': JSON.stringify(body), 'headers': headers, 'method': 'PUT' });
        });

        it('Success PUT method returns promise with data', () => {
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            const body = { test: 'test' };
            let test = instance.put('test.com', body);
            expect(test).toBeInstanceOf(Promise);
            return expect(test).resolves.toEqual(({ data: '12345' }));
        });

        it('Error on PUT method returns promise with APIException', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 400, 'statusText': 'No Auth' });
            let test = instance.put('test.com', {});
            expect(test).rejects.toBeInstanceOf(Promise.reject);
            await expect(test).rejects.toHaveProperty('status', 400);
            await expect(test).rejects.toHaveProperty('statusText', 'No Auth');
        });

        it('401 Error on PUT method returns promise with APIException', async () => {
            global.fetch.mockResponseOnce(JSON.stringify({}), { 'status': 401, 'statusText': 'No Auth' });
            let test = instance.put('test.com', {});
            await expect(test).toBeInstanceOf(Promise);
            expect(auth.login).toBeCalled();
        });
    });

    describe('No token', () => {
        it('wont bind a auth header if no token', () => {
            auth.getToken = jest.fn().mockReturnValue(undefined);
            global.fetch.mockResponseOnce(JSON.stringify({ data: '12345' }), mockResponseInit);
            instance.get('test.com');
            expect(global.fetch).toHaveBeenCalledTimes(1);
            testResponseInit.method = 'GET';
            expect(global.fetch).toHaveBeenCalledWith('test.com', {'headers': {'map': {'accept': 'application/json', 'content-type': 'application/json'}}, 'method': 'GET'});
        });
    });
});