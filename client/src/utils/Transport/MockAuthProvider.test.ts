/// <reference types="jest"/>
import MockAuthProvider from './MockAuthProvider';
describe('APIException Class', () => {
    let instance: MockAuthProvider;
    beforeEach(() => {
        instance = new MockAuthProvider();
    });

    it('has a login method that return resolved promise', async () => {
       const auth = await instance.login();
       expect(auth).toBe(true);
    });
});