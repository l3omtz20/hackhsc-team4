export default class MockAuthProvider {
    public login(): Promise<boolean> {
        const prom = Promise.resolve(true);
        return prom;
    }
    public logout(): Promise<void> {
        const prom = Promise.resolve();
        return prom;
    }
    public getToken(): string | undefined {
        return 'mock-token';
    }
}