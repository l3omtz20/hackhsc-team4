import * as queryString from 'querystringify';
import ITransport from './ITransport';
import MockAuthProvider from '@utils/Transport/MockAuthProvider';

export default class Transport implements ITransport {
    constructor(private authProvider: MockAuthProvider) {
    }

    public get<T>(url: string, params?: { [key: string]: string | undefined }) {
        const qs = params && queryString.stringify(params);
        const full = url + (qs ? '?' + qs : '');
        const options = {
            method: 'GET'
        };
        return this.sendFetch<T>(full, options);
    }

    public delete<T>(url: string, params?: { [key: string]: string | undefined }) {
        const qs = params && queryString.stringify(params);
        const full = url + (qs ? '?' + qs : '');
        const options = {
            method: 'DELETE'
        };
        return this.sendFetch<T>(full, options);
    }

    /* tslint:disable:no-any */
    public post<T>(url: string, body?: any) {
        const options = {
            method: 'POST',
            body: body && JSON.stringify(body)
        };
        return this.sendFetch<T>(url, options);
    }
    /* tslint:disable:no-any */
    public put<T>(url: string, body?: any) {
        const options = {
            method: 'PUT',
            body: body && JSON.stringify(body)
        };
        return this.sendFetch<T>(url, options);
    }

    private async sendFetch<T>(url: string, options: { method: string, body?: any}): Promise<T> {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers = this.appendAuthHeader(headers);
        const sendOptions = Object.assign(options, { headers: headers });
        const res = await fetch(url, sendOptions);

        if (res.ok) {
            if (res.headers.has('content-type') && res.headers.get('content-type')!.indexOf('application/json') === 0) {
                return res.json() as Promise<T>;
            }
            else if (res.headers.has('content-type') && res.headers.get('content-type')!.indexOf('application/zip') === 0) {
                return res.blob() as any as Promise<T>;
            }
            else {
                return res.text() as any as Promise<T>;
            }
        }
        else {
            if (res.status === 401) {
                this.authProvider.logout();
                await this.authProvider.login();
                return Promise.reject(res);
            }
            else {
                return Promise.reject(res);
            }
        }
    }

    private appendAuthHeader(headers: Headers) {
        const token = this.authProvider!.getToken();
        if (token) {
            headers.append('Authorization', 'Bearer ' + token);
        }
        return headers;
    }
}