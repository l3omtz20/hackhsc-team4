export default interface ITransport {
    get<T>(url: string, params?: { [key: string]: string | undefined }): Promise<T>;
    delete<T>(url: string, params?: { [key: string]: string | undefined }): Promise<T>;
    /* tslint:disable:no-any */
    post<T>(url: string, body?: any): Promise<T>;
    /* tslint:disable:no-any */
    put<T>(url: string, body?: any): Promise<T>;
}