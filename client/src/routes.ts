import DashboardPage from './pages/dashboard/Dashboard';
// import { constants } from 'router5';

export default [
    {
        name: 'dashboard',
        path: '/',
        component: DashboardPage
    },
];