/// <reference types="jest"/>
import Transport from '@utils/Transport/Transport';
import { IChartData } from 'stores/DashboardStore/IDashboardStore';
import ChartService from './ChartService';

describe('ListService', () => {
    let instance: ChartService;
    const transport = new Transport();
    beforeEach(() => {
        instance = new ChartService(transport);
    });

    it('has a ajax method getLineChartData', () => {
        transport.get = jest.fn().mockReturnValue(Promise.resolve());
        instance.getLineChartData();
        expect(transport.get).toHaveBeenCalledTimes(1);
        expect(transport.get).toBeCalledWith(`/api/chartdata/`);
    });

    it('has a ajax method getBarChartData', () => {
        transport.get = jest.fn().mockReturnValue(Promise.resolve());
        instance.getBarChartData();
        expect(transport.get).toHaveBeenCalledTimes(1);
        expect(transport.get).toBeCalledWith('/api/detailsdata');
    });
});