import Transport from '@utils/Transport/Transport';
import { IChartData } from 'stores/DashboardStore/IDashboardStore';
 // This service uses the custom transport class which is a wrapper around the native fetch api
export default class ChartService {
    constructor(private transport: Transport) { }
    getLineChartData() {
        return this.transport.get<Array<IChartData>>(`/api/chartdata/`);
    }
    getBarChartData() {
        return this.transport.get<Array<IChartData>>('/api/detailsdata');
    }
}