import Transport from '@utils/Transport/Transport';
import { ITableData } from 'stores/DashboardStore/IDashboardStore';

export default class TableService {
    constructor(private transport: Transport) { }
    getTableData() {
        return this.transport.get<Array<ITableData>>('/api/greek-alphabet');
    }
}