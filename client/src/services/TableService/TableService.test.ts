/// <reference types="jest"/>
import Transport from '@utils/Transport/Transport';
import { ITableData } from '../../stores/DashboardStore/IDashboardStore';
import TableService from './TableService';

describe('TableService', () => {
    let instance: TableService;
    const transport = new Transport();
    beforeEach(() => {
        instance = new TableService(transport);
    });

    it('has a ajax method getLineChartData', () => {
        transport.get = jest.fn().mockReturnValue(Promise.resolve());
        instance.getTableData();
        expect(transport.get).toHaveBeenCalledTimes(1);
        expect(transport.get).toBeCalledWith('/api/greek-alphabet');
    });

});